FROM ubuntu:latest

COPY . /app
WORKDIR /app

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y nginx jq bash git

git clone https://gitlab.gwdg.de/asta/ophase-ws202021.git
